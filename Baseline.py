import os
import os.path
import numpy as np
import time
import string
import pickle
import math

import spacy

from operator import itemgetter

import tensorflow as tf
import tensorflow.contrib.layers as layers
from IPython import embed


from nltk.corpus import stopwords as sw
from nltk.corpus import wordnet as wn
from nltk import wordpunct_tokenize
from nltk import WordNetLemmatizer
from nltk import sent_tokenize

from nltk import pos_tag

from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import SGDClassifier
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.metrics import classification_report as clsr
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cross_validation import train_test_split as tts
from sklearn.metrics import log_loss
from sklearn import metrics
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier

from functools import lru_cache

from scipy.sparse import csr_matrix
from scipy.sparse import vstack
from scipy.sparse import hstack

import itertools
from IPython import embed

def dump_preds(preds):
    with open("stage_2_dump.csv", "w") as f:
        for i, l in enumerate(preds):
            f.write(str(i+1)+","+",".join(map(str,l))+"\n")


def timeit(func):
    """
    Simple timing decorator
    """
    def wrapper(*args, **kwargs):
        start  = time.time()
        result = func(*args, **kwargs)
        delta  = time.time() - start
        return result, delta
    return wrapper


def identity(arg):
    """
    Simple identity function works as a passthrough.
    """
    return arg


# class NLTKPreprocessor(BaseEstimator, TransformerMixin):
class Preprocessor():
    """
    Transforms input data by using NLTK tokenization, lemmatization, and
    other normalization and filtering techniques.
    """

    def __init__(self, stopwords=None, punct=None, lower=True, strip=True, k=2):
        """
        Instantiates the preprocessor, which make load corpora, models, or do
        other time-intenstive NLTK data loading.
        """
        self.lower      = lower
        self.strip      = strip
        self.stopwords  = set(stopwords) if stopwords else set(sw.words('english'))
        self.punct      = set(punct) if punct else set(string.punctuation)
        self.lemmatizer = WordNetLemmatizer()
        self.k = k

    def fit(self, X, y=None):
        """
        Fit simply returns self, no other information is needed.
        """
        return self

    def inverse_transform(self, X):
        """
        No inverse transformation
        """
        return X


    def transform(self, docs, Qs):
        """
        Actually runs the preprocessing on each document.
        """
        return list(self.tokenize_sentences_and_query(doc, q, i) for i,(doc, q) in enumerate(zip(docs,Qs))) 

    def tokenize_sentences_and_query(self, document, query, i=12.5):

        if i%10 == 0:
            print("{} documents tokenized".format(i))

        sentences = sent_tokenize(document)
        n_sent=len(sentences)
        w = {}

        sentences = [" ".join(self.tokenize(sentence)) for sentence in sentences]
        query = " ".join(self.tokenize(query))
        
        return sentences, query


    def top_k_sentences(self, document, query):
        sentences = sent_tokenize(document)
        n_sent=len(sentences)
        w = {}

        sentences = [self.tokenize(sentence) for sentence in sentences]
        query = self.tokenize(query)
        
        # binary independence model
        for term in query:
            occ = len([s for s in sentences if term in s])
            if occ > 0:
                w[term]=(
                    math.log(1+(0.5*n_sent)*(1/occ))
                    ) # num sentences with term
            else:
                w[term]=0

        # Two-Poisson
        # todo
        scores = [0]*n_sent

        for j, sent in enumerate(sentences):
            for i, term in enumerate(query):

                if term in sent:
                    scores[j] += w[term]

        scores = np.array(scores)
        ind = list(map(int,np.argpartition(scores, -self.k)[-self.k:]))
        top_sentences = list(map(sentences.__getitem__, ind))
        return top_sentences


    def tokenize(self, sentence):
        # Break the sentence into part of speech tagged tokens
        tokens = []
        for token, tag in pos_tag(wordpunct_tokenize(sentence)):
            # Apply preprocessing to the token
            token = token.lower() if self.lower else token
            token = token.strip() if self.strip else token
            token = token.strip('_') if self.strip else token
            token = token.strip('*') if self.strip else token

            # If punctuation or stopword, ignore token and continue
            # if token in self.stopwords or all(char in self.punct for char in token):
                # continue
            if all(char in self.punct for char in token):
                continue

            # Lemmatize the token and yield
            lemma = self.lemmatize(token, tag)
            tokens.append(lemma)

        return tokens
            # yield lemma

    def lemmatize(self, token, tag):
        """
        Converts the Penn Treebank tag to a WordNet POS tag, then uses that
        tag to perform much more accurate WordNet lemmatization.
        """
        tag = {
            'N': wn.NOUN,
            'V': wn.VERB,
            'R': wn.ADV,
            'J': wn.ADJ
        }.get(tag[0], wn.NOUN)

        return self.lemmatizer.lemmatize(token, tag)





def load_labels(train=True, text="./training_text_r", variations="./training_variants_r"):

    with open(variations) as variations_file:
        classes = [var.rstrip().split(",")[-1] for var in variations_file] # extract classes from file
        classes = np.array(list(map(int,classes)))-1

    return classes

def load_data(text_path="./training_text_r", variations_path="./training_variants_r"):

    with open(text_path) as text_file:
        texts = list(text_file)
    texts = [text.split("||")[1] for text in texts]

    with open(variations_path) as variations_file:
        variations = [" ".join(var.rstrip().split(",")[1:-1]) for var in variations_file] # take the gene and variation form file and join them with a space

    with open(variations_path) as variations_file:
        classes = [var.rstrip().split(",")[-1] for var in variations_file] # extract classes from file
        classes = np.array(list(map(int,classes)))-1

    return texts, variations, classes



class Retriever():

    def __init__(self, rjecnik=None, k=2):
        print("k:", k)
        self.k=k
        self.pp = Preprocessor(k=self.k)
        self.rjecnik = rjecnik


    def build(self, documents, queries, retrieve=False):
        documents_l, queries_l = self.lemmatize_if_not_pickled(documents,queries)
        self.build_or_load_vectorizer(documents_l, queries_l)
        # self.nlp = spacy.load("en_core_web_sm")

        if retrieve:
            documents_v, queries_v = self.vectorize(documents_l, queries_l)
            X = self.retrieve_from_vectorized(documents_v, queries_v, self.k)
            return X

        else:
            return None


    def build_or_load_vectorizer(self, documents_l, queries_l, pickle_path="./experiment/train_tfidfvectorizer3.pickle"):

        # build ifidf vectorizer
        # sentences = list(itertools.chain(*documents_l))+list(self.rjecnik.values())
        sentences = list(itertools.chain(*documents_l))


        if os.path.isfile(pickle_path):
            print("Loading TfidfVectorizer")
            with open(pickle_path,'rb') as f: self.tfidf = pickle.load(f)
            

        else:
            print("Training TfidfVectorizer")
            self.tfidf = TfidfVectorizer(tokenizer=identity, preprocessor=None, lowercase=False)
            self.tfidf.fit(sentences)
            print("HERE")
            with open(pickle_path, 'wb') as f: pickle.dump(self.tfidf, f)

        print("vectorizer built")


    def lemmatize(self, documents, queries):
        # build lematizer
        print("Lematizing")
        results = self.pp.transform(documents, queries)
        documents_l, queries_l = list(zip(*results))

        print("lematized")
        return documents_l, queries_l

    def lemmatize_if_not_pickled(self, documents, queries, pickle_path="./experiment/train_lemmatized.pickle"):
        if os.path.isfile(pickle_path):
            print("Loading lemmatized")
            with open(pickle_path,'rb') as f: documents_l, queries_l = pickle.load(f)
        else:
            documents_l, queries_l = self.lemmatize(documents, queries)
            with open(pickle_path, "wb") as f: pickle.dump((documents_l, queries_l), f)

        return documents_l, queries_l


    def retrieve(self, documents, queries, k, pickle_path="./experiment/train_lemmatized.pickle"):

        documents_l, queries_l = self.lemmatize_if_not_pickled(documents, queries, pickle_path=pickle_path)

        documents_v, queries_v = self.vectorize(documents_l, queries_l)

        X = self.retrieve_from_vectorized(documents_v, queries_v, k)

        return X


    def vectorize(self, documents_l, queries_l):
        tf_docs = [self.tfidf.transform(doc) for doc in documents_l]

        # tf_docs = [self.my_nlp(i, doc) for i, doc in enumerate(documents_l)]

        tf_qs = self.tfidf.transform(queries_l)
        print("vectorized")
        return tf_docs, tf_qs

    def retrieve_from_vectorized(self, documents_v, queries_v, k):

     
        top_tf_docs = []
        for i, (d, q) in enumerate(zip(documents_v, queries_v)):  

            if i % 10 == 0:
                print("retrieving: ", i)
            sims = metrics.pairwise.cosine_similarity(d,q).T[0]

            if len(sims) > k:
                ind = list(map(int,np.argpartition(sims, k)[-k:]))
                top_tf_docs.append(d[ind])


            else:
                padding = csr_matrix((k-len(sims),d.shape[-1]), dtype=np.float32)
                top_tf_docs.append(vstack((d, padding)))

      
        preprocessed_data = list(vstack((ds, q)) for ds, q in zip(top_tf_docs, queries_v))
      
        # with open("./experiment/pp_docs", "wb") as f: pickle.dump(preprocessed_data, f, pickle.HIGHEST_PROTOCOL)
        
        flattened = [x.tolil().reshape((1,np.prod(x.shape))) for x in preprocessed_data]
 
        X = vstack(flattened).tocsr()
       
        return X


class Model:
    
    def __init__(self, embedding_size=327, sentence_number=30):

        self.embedding_size = embedding_size
        self.sentence_number = sentence_number
        self.input_size = self.embedding_size*(sentence_number+1) # sentences and a query

        # self.X = tf.placeholder(dtype=tf.float32, shape=(None, 98427), name="input")
        # self.indices = tf.placeholder(dtype=tf.int64)
        # self.data = tf.placeholder(dtype=tf.float32)
        # self.shape = tf.placeholder(dtype=tf.int64)

        # self.X = tf.SparseTensor(self.indices, self.data, self.shape)
        self.X = tf.placeholder(tf.float32, shape=(None, self.input_size), name="input")
        self.Y_ = tf.placeholder(tf.int32, shape=(None,), name="labels")
        # self.Q = tf.placeholder(tf.int32, name="queries")


        # net = tf.sparse_tensor_to_dense(self.X)
        # net = tf.sparse_reshape(sp_input=self.X, shape=[-1, 301, 327])
        # net = tf.reshape(self.X, shape=[-1, 301, 327, 1])

        net = tf.reshape(self.X, shape=[-1, self.sentence_number+1, self.embedding_size])

        # net = net[:,:-1,:]
        # query = tf.squeeze(net[:,-1,:])
        # query.set_shape(shape=(None, self.embedding_size))

        def lstm_cell(size): return tf.contrib.rnn.BasicLSTMCell(size)

        rnn_sizes = [100]
        stacked_lstm = tf.contrib.rnn.MultiRNNCell([lstm_cell(s) for s in rnn_sizes])
        net, _  = tf.nn.dynamic_rnn(stacked_lstm, net, dtype=tf.float32)
        
        # net = tf.layers.conv2d(net, 10, kernel_size=[5, 327], padding='VALID', activation=tf.nn.relu, name='conv1')
        # net = tf.layers.conv2d(net, 10, kernel_size=[5, 1], padding='VALID', activation=tf.nn.relu, name='conv2')


        # net = tf.sparse_tensor_to_dense(net)
        # net.set_shape(shape=(None, 301, 327))
        # net = tf.sparse_reduce_max(net, axis=[1]) 

        # net.set_shape(shape=(None, 327))
        # net = tf.convert_to_tensor(net)
        net = tf.reshape(net, shape=[-1, self.sentence_number+1, rnn_sizes[-1], 1])
        net = tf.nn.max_pool(net, [1, self.sentence_number+1, 1, 1], [1, 1, 1, 1], padding='VALID', name='mp1')
        print(net.shape)

        net = layers.flatten(net)
        # net = tf.concat((net, query), axis=1)
        net = layers.fully_connected(net, 200)
        net = layers.fully_connected(net, 100)
        self.logits = layers.fully_connected(net, 9, activation_fn=None)

        self.loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.Y_, logits=self.logits))

        self.train_step = tf.train.AdamOptimizer(0.001).minimize(self.loss)
        self.acc = self.evaluate(self.logits, self.Y_, num_classes=9)

        self.merged_s_op = tf.summary.merge_all()
        self.session = tf.Session()

    def evaluate(self, logits, labels, num_classes):
        confusion_matrix = tf.confusion_matrix(
              labels=labels,
              predictions=tf.argmax(logits, axis=1),
              num_classes=num_classes)

        conf_diag = tf.diag_part(confusion_matrix)

        correct = tf.reduce_sum(conf_diag)
        N = tf.reduce_sum(confusion_matrix)
        accuracy = correct/N

        tf.summary.scalar("Accuracy", accuracy)
        return accuracy

    def my_nlp(i, line):
        if i % 10 == 0:
            print("i:", i)
        return self.nlp(line)


    def train_mb(self, X, Q, Y_, Xval, Qval, Yval_, epoch_num=100, batch_size=25):
        D = X.shape[0]
        n = D // batch_size

        self.session.run(tf.global_variables_initializer())
        begin_step=0

        begin_step = begin_step // n
        batch_size = D//n
        indices=list(range(D))


        def convert_sparse_matrix_to_sparse_tensor(X):
            coo = X.tocoo()
            indices = np.mat([coo.row, coo.col]).transpose()
            return indices, coo.data, coo.shape
                    

        # optimizacijska petlja
        for i in range(begin_step, epoch_num):
            ind_shuf = np.random.permutation(indices)
            X = X[ind_shuf]
            Y_ = Y_[ind_shuf]

            for j in range(n):
                Xbatch = X[j:j+batch_size]
                Ybatch_ = Y_[j:j+batch_size]

                if j == 0: 
                    # validation
                    test_steps = 8
                    n_samples = Xval.shape[0]
                    assert n_samples % test_steps == 0

                    test_batch_size = int(n_samples / test_steps)

                    val_loss = 0
                    acc_e = 0
                    for t in range(test_steps):
                        Xtestbatch = X[t:t+test_batch_size]
                        Ytestbatch_ = Y_[t:t+test_batch_size]

                        val_loss_temp, acc_temp_e = self.session.run([self.loss, self.acc], feed_dict={self.X:Xtestbatch.todense(), self.Y_:Ytestbatch_})

                        val_loss += val_loss_temp
                        acc_e += acc_temp_e

                    val_loss/=test_steps
                    acc_e/=test_steps

                    # self.swriter_validation.add_summary(val_summary, i)
                    print()
                    print("Validation:")
                    print("epoch:", i, "/", epoch_num)
                    print("val_loss:", val_loss, " acc:", acc_e)
                    
                    # training
                    _, loss, acc_e = self.session.run([
                            self.train_step,
                            self.loss,
                            self.acc
                            ],
                            feed_dict={self.X:Xbatch.todense(), self.Y_:Ybatch_}
                    )
                    # self.saver.save(self.session, self.experiment_path, global_step=i)
                    # self.swriter_train.add_summary(train_summary, global_step=i)
                    # print("g_step:", g_step, "learning_rate", lr)
                    print("train_loss:", loss, "AC:", acc_e)

                else:
                    # I,D,S = convert_sparse_matrix_to_sparse_tensor(Xbatch)
                    _ = self.session.run(
                            self.train_step,
                            feed_dict={self.X:Xbatch.todense(), self.Y_:Ybatch_}
                    )

def return_alias_dictionary():

    alias_dict = {}

    with open("./aliases", "r") as f:

        for line in f:

            columns = line.split('\t')

            aliases = []

            columns[2] = columns[2].replace('"', "")
            aliases.extend(columns[2].split("|"))

            columns[8] = columns[8].replace('"', "")
            columns[9] = columns[9].replace('"', "")
            columns[10] = columns[10].replace('"', "")
            columns[11] = columns[11].replace('"', "")

            alias_symbols = columns[8].split("|")
            aliases.extend(alias_symbols)

            alias_names = columns[9].split("|")
            aliases.extend(alias_names)

            prev_symbols = columns[10].split("|")
            aliases.extend(prev_symbols)

            prev_names = columns[11].split("|")
            aliases.extend(prev_names)

            aliases = [x for x in aliases if x != '']
            alias_dict[columns[1]] = aliases

    return alias_dict

if __name__ == "__main__":
    train = True
    # rjecnik = return_alias_dictionary()
    # print(len(rjecnik.items()))

    if train:
        docs, queries, classes = load_data(text_path="./training_text_r", variations_path="./training_variants_r")

        def expand_queries(queries): 
            new_queries= []
            for q in queries:
          
                new_q = q + " driver passanger"
                new_queries.append (new_q)

            return new_queries

        queries = expand_queries(queries)
        rjecnik = None
        
        print("alias_substitutor")

       
       
        for i in range (len(docs)):
            print ("Doc ", i)
        
            alias_dict = return_alias_dictionary()

            parts_of_query = queries[i].split()
            gene = parts_of_query[0]
            
           

            aliases = alias_dict[gene]
            
            
            
            for a in aliases:
                    
                 docs[i] = docs[i].replace(a.lower(), ' ' + gene.lower() + ' ')
            

        retriever = Retriever(rjecnik, k=300)

        if os.path.isfile("./experiment/pp_docs"):
            retriever.build(docs, queries)

            print("LOADING preprocessed data")
            with open("./experiment/pp_docs", "rb") as f:
                X = pickle.load(f)

        else:
            X = retriever.build(docs, queries, retrieve=True)

            with open("./experiment/pp_docs", "wb") as f:
                pickle.dump(X, f, pickle.HIGHEST_PROTOCOL)
        


        print ("TEST")
        # test set
        # docs_v, queries_v, classes_v = load_data(text_path="./test_text_r", variations_path="./test_variants_r")
        docs_v, queries_v, classes_v = load_data(text_path="./stage2_test_text.csv", variations_path="./stage2_test_variants_dummy_cls.csv")

        queries_v = expand_queries(queries_v)

        print("alias_substitutor")

       
       
        for i in range (len(docs_v)):
            
            print ("TEST DOC", i)
            

            parts_of_query = queries_v[i].split()
            gene = parts_of_query[0]
            
           

            aliases = alias_dict[gene]
            
            
            
            for a in aliases:
                    
                 docs_v[i] = docs_v[i].replace(a.lower(), ' ' + gene.lower() + ' ')
            



        if os.path.isfile("./experiment/pp_docs_v"):
            print("LOADING preprocessed val data")
            with open("./experiment/pp_docs_v", "rb") as f:
                Xval = pickle.load(f)

        else:
            Xval = retriever.retrieve(docs_v, queries_v, k=300, pickle_path="./experiment/test_lemmatized.pickle")

            with open("./experiment/pp_docs_v", "wb") as f:
                pickle.dump(Xval, f, pickle.HIGHEST_PROTOCOL)


        print("train:", X.shape)
        print("test:", Xval.shape)


        # fit a classifier
        print("Training model")

        # model = Model()
        # model.train_mb(X=X, Q=None, Y_=classes, Xval=Xval, Qval=None, Yval_=classes_v, epoch_num=100, batch_size=25)

        # model = LogisticRegression(solver='sag')
        model = SGDClassifier(loss="log", learning_rate='constant', eta0=0.0001)
        model.fit(X, classes)
        print(model.score(Xval, classes_v))
        preds = model.predict_proba(Xval)
        dump_preds(preds)
        # loss = log_loss(classes_v, preds)
        # print("Loss: ", loss)

    else:
        docs, queries, classes = load_data(text_path="./test_text_r", variations_path="./test_variants_r")
        with open("./experiment/retriever", "rb") as f:
            floki = pickle.load(f)

        with open("./experiment/lr.sav","rb") as f:
            lr = pickle.load(f)

        X = floki.retrieve(docs, queries, k=30, pickle_path="./experiment/test_lemmatized.pickle")
        print(classes)
        print(lr.score(X, classes))

        
    # model = build_and_evaluate(X,y, outpath=PATH)
    # print(X)
