import os
import os.path
import numpy as np
import time
import string
import pickle
import math

from operator import itemgetter


from nltk.corpus import stopwords as sw
from nltk.corpus import wordnet as wn
from nltk import wordpunct_tokenize
from nltk import WordNetLemmatizer
from nltk import sent_tokenize
from nltk import pos_tag

from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import SGDClassifier
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.metrics import classification_report as clsr
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cross_validation import train_test_split as tts
from sklearn import metrics
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression

from functools import lru_cache

from scipy.sparse import csr_matrix
from scipy.sparse import vstack
from scipy.sparse import hstack

import itertools

def one_hot(Y, num_of_labels=10):
  Yoh=np.zeros((len(Y),10))
  Yoh[range(len(Y)),Y] = 1
  return Yoh

def timeit(func):
    """
    Simple timing decorator
    """
    def wrapper(*args, **kwargs):
        start  = time.time()
        result = func(*args, **kwargs)
        delta  = time.time() - start
        return result, delta
    return wrapper


def identity(arg):
    """
    Simple identity function works as a passthrough.
    """
    return arg


# class NLTKPreprocessor(BaseEstimator, TransformerMixin):
class Preprocessor():
    """
    Transforms input data by using NLTK tokenization, lemmatization, and
    other normalization and filtering techniques.
    """

    def __init__(self, stopwords=None, punct=None, lower=True, strip=True, k=2):
        """
        Instantiates the preprocessor, which make load corpora, models, or do
        other time-intenstive NLTK data loading.
        """
        self.lower      = lower
        self.strip      = strip
        self.stopwords  = set(stopwords) if stopwords else set(sw.words('english'))
        self.punct      = set(punct) if punct else set(string.punctuation)
        self.lemmatizer = WordNetLemmatizer()
        self.k = k

    def fit(self, X, y=None):
        """
        Fit simply returns self, no other information is needed.
        """
        return self

    def inverse_transform(self, X):
        """
        No inverse transformation
        """
        return X


    def transform(self, docs, Qs):
        """
        Actually runs the preprocessing on each document.
        """
        return list(self.tokenize_sentences_and_query(doc, q, i) for i,(doc, q) in enumerate(zip(docs,Qs))) 

    def tokenize_sentences_and_query(self, document, query, i=12.5):

        if i%10 == 0:
            print("{} documents tokenized".format(i))

        sentences = sent_tokenize(document)
        n_sent=len(sentences)
        w = {}

        sentences = [" ".join(self.tokenize(sentence)) for sentence in sentences]
        query = " ".join(self.tokenize(query))
        
        return sentences, query


    def top_k_sentences(self, document, query):
        sentences = sent_tokenize(document)
        n_sent=len(sentences)
        w = {}

        sentences = [self.tokenize(sentence) for sentence in sentences]
        query = self.tokenize(query)
        
        # binary independence model
        for term in query:
            occ = len([s for s in sentences if term in s])
            if occ > 0:
                w[term]=(
                    math.log(1+(0.5*n_sent)*(1/occ))
                    ) # num sentences with term
            else:
                w[term]=0

        # Two-Poisson
        # todo
        scores = [0]*n_sent

        for j, sent in enumerate(sentences):
            for i, term in enumerate(query):

                if term in sent:
                    scores[j] += w[term]

        scores = np.array(scores)
        ind = list(map(int,np.argpartition(scores, -self.k)[-self.k:]))
        top_sentences = list(map(sentences.__getitem__, ind))
        return top_sentences


    def tokenize(self, sentence):
        # Break the sentence into part of speech tagged tokens
        tokens = []
        for token, tag in pos_tag(wordpunct_tokenize(sentence)):
            # Apply preprocessing to the token
            token = token.lower() if self.lower else token
            token = token.strip() if self.strip else token
            token = token.strip('_') if self.strip else token
            token = token.strip('*') if self.strip else token

            # If punctuation or stopword, ignore token and continue
            # if token in self.stopwords or all(char in self.punct for char in token):
                # continue
            if all(char in self.punct for char in token):
                continue

            # Lemmatize the token and yield
            lemma = self.lemmatize(token, tag)
            tokens.append(lemma)

        return tokens
            # yield lemma

    def lemmatize(self, token, tag):
        """
        Converts the Penn Treebank tag to a WordNet POS tag, then uses that
        tag to perform much more accurate WordNet lemmatization.
        """
        tag = {
            'N': wn.NOUN,
            'V': wn.VERB,
            'R': wn.ADV,
            'J': wn.ADJ
        }.get(tag[0], wn.NOUN)

        return self.lemmatizer.lemmatize(token, tag)





def load_labels(train=True, text="./training_text_r", variations="./training_variants_r"):

    with open(variations) as variations_file:
        classes = [var.rstrip().split(",")[-1] for var in variations_file] # extract classes from file
        classes = np.array(list(map(int,classes)))

    return classes

def load_data(text_path="./training_text_r", variations_path="./training_variants_r"):

    with open(text_path) as text_file:
        texts = list(text_file)
    texts = [text.split("||")[1] for text in texts]

    with open(variations_path) as variations_file:
        variations = [" ".join(var.rstrip().split(",")[1:-1]) for var in variations_file] # take the gene and variation form file and join them with a space

    with open(variations_path) as variations_file:
        classes = [var.rstrip().split(",")[-1] for var in variations_file] # extract classes from file
        classes = np.array(one_hot(list(map(int,classes))))

    return texts, variations, classes



class Retriever():

    def __init__(self, k=2):
        print("k:", k)
        self.k=k
        self.pp = Preprocessor(k=self.k)


    def build(self, documents, queries, retrieve=False):
        documents_l, queries_l = self.lemmatize_if_not_pickled(documents, queries, pickle_path="./experiment/train_lemmatized.pickle")

        if retrieve:
            documents_v, queries_v = self.vectorize(documents_l, queries_l)
            X = self.retrieve_from_vectorized(documents_v, queries_v, self.k)
            return X

        else:
            return None


    def build_or_load_vectorizer(self, documents_l, queries_l, pickle_path="./experiment/train_tfidfvectorizer.pickle"):

        # build ifidf vectorizer
        sentences = list(itertools.chain(*documents_l))


        if os.path.isfile(pickle_path):
            print("Loading TfidfVectorizer")
            with open(pickle_path,'rb') as f: self.tfidf = pickle.load(f)

        else:
            print("Training TfidfVectorizer")
            self.tfidf = TfidfVectorizer(tokenizer=identity, preprocessor=None, lowercase=False)
            self.tfidf.fit(sentences)

            with open(pickle_path, 'wb') as f: pickle.dump(self.tfidf, f)

        print("vectorizer built")


    def lemmatize(self, documents, queries):
        # build lematizer
        print("Lematizing")
        results = self.pp.transform(documents, queries)
        documents_l, queries_l = list(zip(*results))

        print("lematized")
        return documents_l, queries_l

    def lemmatize_if_not_pickled(self, documents, queries, pickle_path="./experiment/train_lemmatized.pickle"):
        if os.path.isfile(pickle_path):
            print("Loading lemmatized")
            with open(pickle_path,'rb') as f: documents_l, queries_l = pickle.load(f)
        else:
            documents_l, queries_l = self.lemmatize(documents, queries)
            with open(pickle_path, "wb") as f: pickle.dump((documents_l, queries_l), f)

        return documents_l, queries_l


    def retrieve(self, documents, queries, k, pickle_path="./experiment/train_lemmatized.pickle"):

        documents_l, queries_l = self.lemmatize_if_not_pickled(documents, queries, pickle_path=pickle_path)

        documents_v, queries_v = self.vectorize(documents_l, queries_l)

        X = self.retrieve_from_vectorized(documents_v, queries_v, k)

        return X


    def vectorize(self, documents_l, queries_l):
        tf_docs = [self.tfidf.transform(doc) for doc in documents_l]
        tf_qs = self.tfidf.transform(queries_l)
        print("vectorized")
        return tf_docs, tf_qs

    def retrieve_from_vectorized(self, documents_v, queries_v, k):

        
        top_tf_docs = []
        for i, (d, q) in enumerate(zip(documents_v, queries_v)):  
            sims = metrics.pairwise.cosine_similarity(d,q).T[0]

            if len(sims) > k:
                ind = list(map(int,np.argpartition(sims, -k)[-k:]))
                top_tf_docs.append(d[ind])


            else:
                padding = csr_matrix((k-len(sims),d.shape[-1]), dtype=np.float32)
                top_tf_docs.append(vstack((d, padding)))

        
        preprocessed_data = list(vstack((ds, q)) for ds, q in zip(top_tf_docs, queries_v))

        # with open("./experiment/pp_docs", "wb") as f: pickle.dump(preprocessed_data, f, pickle.HIGHEST_PROTOCOL)

        flattened = [x.tolil().reshape((1,np.prod(x.shape))) for x in preprocessed_data]
        X = vstack(flattened).tocsr()

        return X

def return_alias_dictionary():
    
    f = open ('./aliases', 'r')

    text = f.read()

    #print (text)

    rjecnik = {}

    lista = text.split ('\n')
    prvi = lista[0].split('\t')
    #for i in  prvi:
     #   print (i)

    #print()
    for i in range (1, len(lista)):
        #print (i, lista[i])
        stupci = lista[i].split ('\t')
        
        #for j in range (len(stupci)):
            #print (prvi[j], ":",  stupci[j])
        #print()
        aliasi = []
        #print (stupci)
        stupci[2] = stupci[2].replace('"', "")
        aliasi.extend (stupci[2].split ("|"))
        stupci[8] = stupci[8].replace('"', "");
        stupci[9] = stupci[9].replace('"', "");
        stupci[10] = stupci[10].replace('"', "");
        stupci[11] = stupci[11].replace('"', "");
        alias_symbols = stupci[8].split ("|")
        aliasi.extend(alias_symbols)
        alias_names = stupci[9].split ("|")
        aliasi.extend(alias_names)
        prev_symbols = stupci[10].split ("|")
        aliasi.extend(prev_symbols)
        prev_names = stupci[11].split ("|")
        aliasi.extend(prev_names)
        aliasi = [x for x in aliasi if x != '']
        rjecnik[stupci[1]] = aliasi
        
        
        
        

    for key in rjecnik:
        print (key, ":", rjecnik[key])

    return rjecnik

if __name__ == "__main__":
    train = True
    rjecnik = return_alias_dictionary()

    if train:
        docs, queries, classes = load_data()
        new_queries= []

        for q in queries:
            lista = q.split()
            gene_symbol = lista[0]
            variation= lista[1]
            if gene_symbol not in rjecnik:
                break
            aliasi = rjecnik[gene_symbol]
            alString = ' '.join(aliasi)
            new_q = gene_symbol + alString + variation
            new_queries.append (new_q)

        queries = new_queries

        retriever = Retriever(300)

        if os.path.isfile("./experiment/pp_docs"):
            retriever.build(docs, queries)

            print("LOADING preprocessed data")
            with open("./experiment/pp_docs", "rb") as f:
                X = pickle.load(f)

        else:
            X = retriever.build(docs, queries, retrieve=True)

            with open("./experiment/pp_docs", "wb") as f:
                pickle.dump(X, f, pickle.HIGHEST_PROTOCOL)


        with open("./experiment/retriever", "wb") as f:
            pickle.dump(retriever, f)


        print("X:", X.shape)
        print("classes:", classes.shape)

        # fit a classifier
        if os.path.isfile("./experiment/SVM.sav"):
            print("Loading model")
            with open("./experiment/SVM.sav", 'rb') as f:
                lr = pickle.load(f)

        else:
            print("Training model")
            svm = SVC(C=0.9, decision_function_shape='ovr')
            svm.fit(X, classes)
            pickle.dump(svm, open("./experiment/SVM.sav","wb"))

        print(svm.score(X, classes))

    else:
        docs, queries, classes = load_data(text_path="./test_text_r", variations_path="./test_variants_r")
        with open("./experiment/retriever", "rb") as f:
            floki = pickle.load(f)

        with open("./experiment/lr.sav","rb") as f:
            lr = pickle.load(f)

        X = floki.retrieve(docs, queries, k=300, pickle_path="./experiment/test_lemmatized.pickle")
        print(lr.score(X, classes))
        
        
    # model = build_and_evaluate(X,y, outpath=PATH)
    # print(X)
