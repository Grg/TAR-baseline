import pickle
from nltk import wordpunct_tokenize
from nltk.corpus  import stopwords as sw

with open("./aliasi_big.txt", "rb") as f:
    cont = str(f.read())
    aliases = set(wordpunct_tokenize(cont))

with open("./experiment/train_lemmatized.pickle", "rb") as f:
    documents_l, queries_l = pickle.load(f)

    train = set()
    for doc in documents_l:
        for sent in doc:
            train.update(wordpunct_tokenize(sent))
    print("train len:", len(train))

with open("./experiment/test_lemmatized.pickle", "rb") as f:
    documents_l, queries_l = pickle.load(f)

    test = set()
    for doc in documents_l:
        for sent in doc:
            test.update(wordpunct_tokenize(sent))

    print("test len:", len(test))
    print("trian inter test:", len(set.intersection(train,test)))

    print("train - test: ", len(train.difference(test)))
    tetr = test.difference(train)
    print("test - train: ", len(tetr))
    # tetrsw = tetr.difference(set(sw.words('english')))
    # print("test - train - sw: ", len(tetrsw))
    tetra = tetr.difference(aliases)
    print("test - train - aliases", len(tetra))
    print("coverage: ", len( set.intersection(set.union(train, aliases),test))/len(test) )

        



